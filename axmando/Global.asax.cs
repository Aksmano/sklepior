﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace axmando
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Application.Add("usersOnline", 0);
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            Application.Set("usersOnline",Convert.ToInt32(Application.Get("usersOnline").ToString()) + 1);
        }
        protected void Session_End(object sender, EventArgs e)
        {
            Application.Set("usersOnline", Convert.ToInt32(Application.Get("usersOnline").ToString()) - 1);
        }
    }
}